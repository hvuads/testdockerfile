FROM python:3.9

WORKDIR /code
COPY ./requirements.txt /tmp/requirements.txt
RUN apt-get update -y && apt-get install vim -y
RUN python -m pip install --upgrade pip
RUN pip install --no-cache-dir --upgrade -r /tmp/requirements.txt
RUN rm -f /tmp/requirements.txt
ADD ./main.py /code/main.py
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000", "--reload"]
