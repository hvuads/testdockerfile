# ver := $(shell node -p "require('./package').version")
ver := 1.0.0
run:
	echo "Makefile"
init:
	docker run --privileged --rm tonistiigi/binfmt --install all
	docker buildx create --use
multi-build:
	docker buildx build --provenance false --platform linux/amd64,linux/arm64 -t registry.gitlab.com/hvuads/testdockerfile:$(ver) --push .
