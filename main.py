#from typing import Union
#from typing import List
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from transformers import AutoTokenizer, AutoModelForQuestionAnswering
import torch
import requests
import json
import pandas as pd
#import urllib.parse

app = FastAPI()
origins = [
        "http://localhost",
	"http://bigdata.softnix.co.th/",
	"code.jquery.com",
	"*"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)



class Item(BaseModel):
    context: str
    q: str

    class Config:
        schema_extra = {
            "example": {
                "context": "SOFTNIX TECHNOLOGY CO.,LTD. ก่อตั้งขึ้นในปี 2007 โดยกลุ่ม SYSTEM ENGINEER ที่เชี่ยวชาญด้าน IT OPERATION และ SECURITY โดยมีแรงบันดาลใจที่ต้องการเห็น SOFTWARE ENTERPRISE ฝีมือคนไทยเป็นที่ยอมรับระดับโลก  โดยเราเริ่มต้นจากการพัฒนาซอฟท์แวร์ให้บริการ EMAIL SERVER ที่ใช้ในองค์กรเพราะเป็นช่องทางการสื่อสารธุรกิจที่สำคัญ และตามด้วย LOG MANAGEMENT SYSTEM และ AUTHENTICATION SYSTEM เพราะเราเริ่มมองเห็นว่า เป็นพืนฐานที่ใช้ในระบบเครือข่ายคอมพิวเตอร์ และเติบโตได้จากการขยายตัวของเครือข่ายคอมพิวเตอร์ ซึ่งตลอดระยะเวลาทำให้เราสะสมประสบการณ์และความเชี่ยวชาญในการพัฒนาซอฟท์แวร์ที่ทำงานกับข้อมูลปริมาณมหาศาลตลอดเวลา (REAL TIME DATA) และเมื่อโลกก้าวเข้าสู่ยุค BIG DATA และ AI ทักษะที่สำคัญที่สุดของเรา จึงนำมาสร้าง BIG DATA ANALYTIC PLATFORM ที่ชื่อ SOFTNIX DATA PLATFORM และนำเราก้าวเข้าสู่การเป็น BIG DATA PLATFORM COMPANY ในปัจจุบัน",
                "q": "Softnix ก่อตั้งขึ้นในปีใด",
            }
        }


@app.get("/")
def read_root():
    return {"ThaiQA": "Softnix"}

@app.post("/qa")
async def qa(item: Item):
    q = item.q
    t = item.context
    tokenizer = AutoTokenizer.from_pretrained("Thammarak/wangchanBERTa-QA-thaiqa_squad")
    model = AutoModelForQuestionAnswering.from_pretrained("Thammarak/wangchanBERTa-QA-thaiqa_squad")
    inputs = tokenizer(q,t, return_tensors="pt")
    with torch.no_grad():
        outputs = model(**inputs)
    answer_start_index = outputs.start_logits.argmax()
    answer_end_index = outputs.end_logits.argmax()
    predict_answer_tokens = inputs.input_ids[0, answer_start_index : answer_end_index + 1]
    o = tokenizer.decode(predict_answer_tokens, skip_special_tokens=True)
    return {"result":o}



